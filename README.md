# Ethereum v1 and v2 on MANTRA DAO

- ETH v1 uses `geth` as the build for the PoW implementation
- ETH v2 uses `prysm` as the beacon and validator builds for the PoS implementation

## Submodules

- [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum) @ `v1.10.17`
- [prysmaticlabs/prysm](https://github.com/prysmaticlabs/prysm) @ `v2.0.6` (not built!)
